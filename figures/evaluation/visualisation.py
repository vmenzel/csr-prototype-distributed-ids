# visualisation.py

# Load Matplotlib and data wrangling libraries.
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv

with open('./outputs/DSN/LM1_s2.csv','r') as scenariofile:

            scenario = csv.reader(scenariofile, delimiter = ';')

            timesteps_1 = []
            timesteps_3 = []
            timesteps_5 = []
            s_1 = []         
            s_3 = []
            s_5 = []
            attack=[] 

            for i in range(150):
                
                row = scenario.__next__()

                if (row[0] == "x"): 
                    #break
                    attack.append(i)
                    row = scenario.__next__()


                s_1.append(float(row[0]))
                timesteps_1.append(i)


                #if(i<18): 
                 #   timesteps_1.append(i)
                  #  s_1.append(float(row[0]))

                #if(i<50): 
                 #   timesteps_3.append(i)
                  #  s_3.append(float(row[1]))

                #timesteps_5.append(i)
                #s_5.append(float(row[2]))
                
            #for i in range(len(timesteps_1)): 
             #   timesteps_1[i] = timesteps_1[i] * 4.36

            #for i in range(len(timesteps_3)): 
             #   timesteps_3[i] = timesteps_3[i] * 1.62

            



mark = [timesteps_1.index(i) for i in attack]
print(attack)
print(len(attack))

plt.plot(attack,[s_1[i] for i in mark], color = "r", ls="", marker="o", label="Alert")

plt.plot(timesteps_1, s_1, color='b', marker = '', label="Evaluation time per data set")





plt.rc('legend',fontsize=20) # using a size in points
#plt.rc('legend',fontsize='medium') # using a named size

#plt.legend(fontsize=50)
#plt.gca().invert_yaxis()

#plt.title("Scenario 2 - Local Monitor 1", size=28)
plt.xlabel("Evaluation step", fontsize = 30)
plt.ylabel("Evaluation time (in sec)", fontsize = 30)

plt.legend(loc='upper left')

#plt.ylim(bottom=-2, top=10) 

#plt.yticks([])
#plt.legend()
   
# Show the plot
plt.show()