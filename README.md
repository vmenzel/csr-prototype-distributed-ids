# Securing SCADA networks for smart grids via a distributed evaluation of local sensor data

Recent incidents clearly identify the need for improved (cyber) security in the power distribution grid. 
The communication infrastructure of a power grid (the Supervisory Control and Data Acquisition (SCADA) network) is often a lucrative target for cyber-attacks and manipulations. 
In a recent line of work, a process-aware approach was proposed to locally monitor the communicated data and detect anomalies and inconsistencies.
Recently, that approach was extended to a neighborhood level  and tested in a simulation environment.  
This paper takes the idea closer to practice and shows the feasibility of the extended approach on distributed hardware. 
We evaluate the hardware capacity, the chosen communication protocol, and the real-time capability with respect to performance on a Raspberry Pis cluster and compare it to the originally centralized test cases.
Therefore, in this repository, we adapted the original testbed and IDS to be executable on individual Raspberry Pis.  
The original repository for single host execution can be found [here](https://gitlab.utwente.nl/vmenzel/distributed-smart-grid-ids) and the repository containing the original scenario files can be found [here](https://gitlab.utwente.nl/vmenzel/distributed_ids_prototype).


## Directory Overview
- **ids**: Implementation of networked IDS
  - **contrib**: collection of utility scripts
  - **deployment**: Configuration of `Docker`-based distributed deployment. (See [deployment/README](ids/deployment/README.md) for details)
  - **implementation**: Implementation of IDS System. (See [implementation/README](ids/implementation/README.md) for details)
  - **visualization**: Interactive Visualization of IDS. (See [visualization/README](ids/visualization/README.md) for details)
- **NOTICE**: Third Party libraries and their licenses
- **attack-tool** : The attack tool 
- **logs**: raw log files used for the evaluation 
- **figures**: various figures and adapted evaluation log files used in the paper and during the research


## License
Individual Licenses for the different parts can be found in the respective directories.

## Funding
This research is conducted as part of the ISoLATE project (CS.016) funded by NWO.

## Thanks 
The author would like to thank our industry partners Coteq netbeheer and Smart State Technology for their insights and looking forward to further collaboration to conduct real life test cases. The author would like to thank Piet Björn Adick, Hassan Alamou, Rasim Gürkam Almaz, Lisa Angold, Ben Brooksnieder, Tom Deuschle, Daniel Krug, Gelieza Kötterheinrich, Linus Lehbrink, Justus Pancke and Jan Speckamp for their work on implementing parts of the Mosaik simulation, the attack tool and the IDS as part of their Bachelor and Master in Computer Science at the Westfälische Wilhelms-Universität, Germany.

## Authors and Citation 
The theoretical foundations and background of the repository are described in: 

V. Menzel, K. O. GroBhanten and A. Remke, "Evaluating a Process-Aware IDS for Smart Grids on Distributed Hardware," 2023 IEEE International Conference on Cyber Security and Resilience (CSR), Venice, Italy, 2023, pp. 418-425, doi: 10.1109/CSR57506.2023.10224985.